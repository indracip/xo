function xo(str) {
  // you can only write your code here!
  return (
    str.toLowerCase().match(/x/g).length ===
    str.toLowerCase().match(/o/g).length
  );
}

// TEST CASES
console.log(xo("xoxoxo")); // true
console.log(xo("oxooxo")); // false
console.log(xo("oxo")); // false
console.log(xo("xxxooo")); // true
console.log(xo("xoxooxxo")); // true
